{pkgs,lib, ...}: {
  programs.ausweisapp = {
    enable = true;
    openFirewall = true;
  };
  programs.niri.enable = true;
  programs.xwayland.enable = lib.mkForce true;

  environment.systemPackages = with pkgs; [
    # applications
    qdirstat
    strongswan
    # support
    cached-nix-shell
    protobuf
    alsa-utils
  ];
}

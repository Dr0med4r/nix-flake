{
  config,
  pkgs,
  hostname,
  username,
  system,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./laptop-packages.nix
  ];

  modules.games.enable = true;
  modules.office = {
    enable = true;
    latex = true;
  };
  modules.programming.enable = true;
  modules.console-applications.enable = true;
  modules.fingerprint.enable = true;
  modules.battery-warning.enable = true;
  modules.hyprland.enable = true;
  modules.kanata.enable = true;
  home-modules.stylix.enable = true;
  home-modules.alacritty.enable = true;
  home-modules.hyprland = let
    scale = "1";
  in {
    enable = true;
    scale = scale;
    monitors = ["eDP-1,1920x1080@60,0x0,${scale}" ",preferred,auto,${scale}"];
    extraDevices = [
      {
        name = "kanata";
        kb_variant = "e2";
      }
    ];
  };
  home-modules.zellij.enable = true;

  programs.ydotool.enable = true;
  users.users.${username}.extraGroups = ["ydotool"];

  # Bootloader.
  boot = {
    loader.grub = {
      enable = true;
      device = "/dev/nvme0n1";
      useOSProber = true;
    };
    initrd.kernelModules = ["i915"];
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = ["kvm-intel" "v4l2loopback"];
    extraModulePackages = with config.boot.kernelPackages; [v4l2loopback];
  };

  networking = {
    hostName = hostname;
    networkmanager.enable = true;
    firewall = {
      enable = false;
    };
  };

  services.displayManager.sddm = {
    theme = "breeze";
    wayland.enable = true;
  };

  services.xserver = {
    enable = true;
    xkb.variant = "e2";
    excludePackages = with pkgs; [
      xterm
    ];
  };

  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  services = {
    auto-cpufreq.enable = true;
  };

  virtualisation.kvmgt.enable = true;
  # and opengl

  environment.variables = {
    VDPAU_DRIVER = "va_gl";
  };

  nix = {
    distributedBuilds = false; # use --builders "ssh-ng://remote-build"
    buildMachines = [
      {
        speedFactor = 4;
        hostName = "remote-build";
        system = system;
        protocol = "ssh-ng";
        supportedFeatures = ["nixos-test" "benchmark" "big-parallel" "kvm"];
      }
    ];
    settings = {
      trusted-substituters = ["ssh-ng://remote-build"];
      trusted-public-keys = ["remote-build:O2fhkUdcCtpJTOlDYcoiiPsRV+l9ZJssiNtsv/bLOiw="];
    };
  };

  hardware = {
    bluetooth = {
      enable = true;
    };
    graphics = {
      enable = true;
      extraPackages = with pkgs; [
        intel-vaapi-driver
        intel-media-driver
        libvdpau-va-gl
      ];
    };
  };
}

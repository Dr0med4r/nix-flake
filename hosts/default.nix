{
  inputs,
  home-manager,
  fingerprint-sensor,
  self,
  ...
}: let
  lib = inputs.nixpkgs.lib;
  username = "jakob";
  system = "x86_64-linux";
  common-modules = [
    home-manager.nixosModules.home-manager
    inputs.stylix.nixosModules.stylix
    inputs.nix-index-database.nixosModules.nix-index
    inputs.lix-module.nixosModules.default
    inputs.nvim.homeManagerModules.nvim
    ../modules/system
    ../modules/home
  ];
  common-args = {
    inherit
      lib
      home-manager
      system
      username
      inputs
      self
      ;
  };
in {
  profugus = lib.nixosSystem {
    inherit system;
    specialArgs =
      common-args
      // {
        hostname = "profugus"; # erratic
      };
    modules =
      common-modules
      ++ [
        ./desktop/desktop-configuration.nix
        ./desktop/desktop-home.nix
      ];
  };

  tardus = lib.nixosSystem {
    inherit system;
    specialArgs =
      common-args
      // {
        inherit fingerprint-sensor;
        hostname = "tardus"; # slow
      };
    modules =
      common-modules
      ++ [
        ./laptop/laptop-configuration.nix
        ./laptop/laptop-home.nix
        ../modules/system/fprint.nix
      ];
  };

  test = lib.nixosSystem {
    inherit system;
    specialArgs =
      common-args
      // {
        hostname = "test";
      };
    modules = [
      ./test/test-configuration.nix
    ];
  };

  iso = lib.nixosSystem {
    inherit system;
    specialArgs =
      common-args;
    modules = [
      inputs.nix-index-database.nixosModules.nix-index
      inputs.lix-module.nixosModules.default
      ../modules/system
      ./iso/configuration.nix
    ];
  };
}

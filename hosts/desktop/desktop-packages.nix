{pkgs, ...}: {
  programs = {
    xwayland.enable = true;
    openvpn3.enable = true;
  };
  environment.systemPackages = with pkgs; [
    pkg-config
    xorg.xdpyinfo
    glxinfo
    xclip
    egl-wayland # nvidia
  ];
}

{
  pkgs,
  username,
  inputs,
  system,
  hostname,
  self,
  ...
}: {
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = {inherit pkgs username inputs system hostname self;};

    # system user
    users.${username} = {
    };

    # root user
    users.root = {...}: {
      home = {
        stateVersion = "23.05";
      };
    };
  };
}

# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  lib,
  pkgs,
  hostname,
  self,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ./desktop-packages.nix
  ];

  modules.games.enable = true;
  modules.office = {
    enable = true;
    latex = true;
  };
  modules.console-applications.enable = true;
  modules.programming.enable = true;
  modules.games.all = true;
  modules.kanata.enable = true;
  modules.remoteBuild.enableServer = true;
  home-modules.zellij.enable = true;
  home-modules.stylix.enable = true;
  modules.hyprland = {
    enable = true;
    nvidia = true;
  };
  home-modules.alacritty.enable = true;
  home-modules.hyprland = {
    enable = true;
    nvidia = true;
    scale = "1";
    monitors = [
      "Unknown-1, disable"
      "HDMI-A-1,highrr,0x0,1"
      "DVI-D-1,1920x1080@60,-1920x0,1"
    ];
  };

  # Bootloader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = ["v4l2loopback"];
    loader = {
      efi = {
        canTouchEfiVariables = true;
      };
      grub = {
        # use this to enable changing things from legacy to uefi efiInstallAsRemovable = true;
        device = "nodev";
        efiSupport = true;
        useOSProber = true;
        enable = true;
      };
    };
    supportedFilesystems = ["ntfs"];
    extraModulePackages = with config.boot.kernelPackages; [v4l2loopback];
  };

  services.displayManager.sddm = {
    theme = "breeze";
    wayland.enable = true;
  };

  networking.hostName = hostname; # Define your hostname.

  # Enable networking
  networking = {
    networkmanager.enable = true;
  };

  # Enable the X11 windowing system.
  # and Nvidia drivers
  services.xserver = {
    enable = true;
    videoDrivers = ["nvidia"];
  };

  # dont make boot wait on network
  systemd.services.NetworkManager-wait-online.enable = false;

  hardware = {
    nvidia = {
      open = false;
      modesetting.enable = true;
      nvidiaSettings = true;
      powerManagement.enable = false;
      powerManagement.finegrained = false;
      package = config.boot.kernelPackages.nvidiaPackages.beta;
    };
    graphics = {
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        vaapiVdpau
      ];
    };
  };

  # Configure keymap in X11
  services.xserver = {
    xkb = {
      layout = "de";
      variant = "";
    };
  };

  # Configure console keymap
  console = {
    keyMap = "de";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  environment.etc."where_config".text = toString self;

  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };
}

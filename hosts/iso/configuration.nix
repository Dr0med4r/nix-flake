{
  pkgs,
  modulesPath,
  system,
  ...
}: {
  imports = [
    "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix"
  ];
  nixpkgs.hostPlatform = system;
  modules.kanata.enable = true;
  boot.loader.grub.efiInstallAsRemovable = true;

  environment.systemPackages = with pkgs; [
    neovim
    git
  ];
}

{
  home-manager,
  username,
  hostname,
  self,
  ...
}:
home-manager.nixosModules.home-manager {
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = {inherit username hostname self;};
    users.${username} = {
    };
    users.root = {
      pkgs,
      config,
      ...
    }: {
      home = {
        stateVersion = "23.05";
      };
    };
  };
}

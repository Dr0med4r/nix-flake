{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    # console
    pciutils
    usbutils
    xcowsay
    freshfetch
    unzip
    wget
    git
    file
    zip
    zoxide
    ripgrep
    gnome.gnome-software

    # applications
    firefox
    opentabletdriver
  ];
}

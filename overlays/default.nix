{
  inputs,
  system,
  pkgs,
  ...
}: {
  hyproverlay = import ./hyproverlay.nix {inherit inputs system;};
  openvpn = import ./openvpn.nix {};
  flutteroverlay = import ./flutteroverlay.nix {inherit pkgs;};
}

{
  inputs,
  system,
  ...
}: let
  hyprpkg = inputs.hyprland.packages.${system}.hyprland;
in
  final: prev: {
    hyprshade = prev.hyprshade.override {hyprland = hyprpkg;};
    waybar = prev.waybar.override {
      hyprland = hyprpkg;
      swaySupport = false;
    };
  }

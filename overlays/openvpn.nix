{}: (final: prev: {
  openvpn3 = prev.openvpn3.overrideAttrs (old: {
    patches =
      (old.patches or [])
      ++ [
        ./openvpn.patch # point to wherever you have this file, or use something like `fetchpatch`
      ];
  });
})

{
  lib,
  pkgs,
  config,
  ...
}: let
  cfg = config.modules.games;
in {
  options.modules.games = {
    enable = lib.mkEnableOption "enable games";
    all = lib.mkEnableOption "enable all games";
  };

  config = lib.mkIf cfg.enable {
    networking.firewall = {
      # for supertuxkart
      allowedTCPPorts = [2759 2757];
      allowedUDPPorts = [2759 2757];
    };
    environment.systemPackages = with pkgs;
      [
        prismlauncher
        superTuxKart
        # for wayland needs NIXOS_OZONE_WL=1
        vesktop
      ]
      ++ lib.optionals cfg.all [
        # osu-lazer-bin
        vulkan-tools
        lutris
        wine
        winetricks
      ];
    programs.steam.enable = cfg.all;
  };
}

{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.modules.remoteBuild;
in {
  options.modules.remoteBuild = {
    enableServer = lib.mkEnableOption "enable building on this system";
  };
  config = lib.mkIf cfg.enableServer {
    users.users.remote-build = {
      isNormalUser = true;
      shell = pkgs.bash;
      homeMode = "555";
    };
    networking.firewall.extraCommands = ''
      iptables \
      --append INPUT \
      --protocol tcp \
      --match tcp \
      --destination-port 22 \
      --match iprange \
      --src-range 100.64.0.0-100.64.0.255 \
      --match comment \
      --comment "ssh + sshfs" \
      --jump ACCEPT'';
    services.openssh = {
      enable = true;
      settings = {
        AllowUsers = ["remote-build"];
        PasswordAuthentication = false;
      };
    };
  };
}

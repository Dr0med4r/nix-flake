{...}: {
  imports = [
    ./common-applications.nix
    ./games.nix
    ./office.nix
    ./programming.nix
    ./battery-warning.nix
    ./common-configuration.nix
    ./console-applications.nix
    ./hyprland.nix
    ./nix-config.nix
    ./kanata.nix
    ./remote-build.nix
  ];
}

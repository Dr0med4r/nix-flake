{
  pkgs,
  lib,
  config,
  username,
  ...
}: let
  cfg = config.modules.programming;
in {
  options.modules.programming = {
    enable = lib.mkEnableOption "enable programming";
  };
  config = lib.mkIf cfg.enable {
    documentation.dev.enable = true;
    users.users.${username}.extraGroups = ["docker"];
    virtualisation.docker.enable = true;
    environment.systemPackages = with pkgs; [
      gnumake
      dart
      gcc
      gdb
      go
      man-pages
      man-pages-posix
      (python3.withPackages (ps:
        with ps; [
          requests
          matplotlib
          pyarrow
          pandas
          tkinter
          pip
        ]))
      black
      rustc
      cargo
      rustup
      rustfmt
      gradle
      patchelf
      gnumake
      jdk
      typescript
      bun
      nix-inspect
      k9s
      kubectl
    ];
  };
}

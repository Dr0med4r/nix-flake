{
  inputs,
  config,
  system,
  pkgs,
  lib,
  ...
}: let
  cfg = config.modules.hyprland;
  hyprlandpkgs = inputs.hyprland.packages.${system};
in {
  options.modules.hyprland = {
    enable = lib.mkEnableOption "enable hyprland";
    nvidia = lib.mkEnableOption "enable nvidia";
  };

  config = lib.mkIf cfg.enable (lib.mkMerge [
    {
      # use polkit
      security.polkit.enable = true;
      # for yubico as https://github.com/NixOS/nixpkgs/issues/121121#issuecomment-1918232797 stated
      environment.systemPackages = with pkgs; [
        pcscliteWithPolkit.out
        wl-clipboard
        dbus
        networkmanagerapplet
      ];
      services.logind = {
        powerKey = "ignore";
        powerKeyLongPress = "poweroff";
      };
      programs.hyprland = {
        enable = true;
        withUWSM = true;
        package = hyprlandpkgs.hyprland;
        portalPackage = hyprlandpkgs.xdg-desktop-portal-hyprland;
      };
      xdg.portal = {
        enable = true;
        xdgOpenUsePortal = true;
        extraPortals = [
          hyprlandpkgs.xdg-desktop-portal-hyprland
          pkgs.xdg-desktop-portal-gtk
        ];
      };
      environment.variables = {
        TERM = "alacritty";
        TERMINAL = "alacritty";
        NIXOS_OZONE_WL = "1";
      };

      nix.settings = {
        substituters = ["https://hyprland.cachix.org"];
        trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
      };
    }
    (lib.mkIf
      cfg.nvidia {
        environment.variables = {
          LIBVA_DRIVER_NAME = "nvidia";
          XDG_SESSION_TYPE = "wayland";
          __GLX_VENDOR_LIBRARY_NAME = "nvidia";
          AQ_DRM_DEVICES = "/dev/dri/card1:/dev/dri/card0";
          GBM_BACKEND = "nvidia-drm";
          NVD_BACKEND = "direct";
        };
      })
  ]);
}

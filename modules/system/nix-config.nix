{
  pkgs,
  lib,
  home-manager,
  system,
  username,
  inputs,
  self,
  ...
}: let
  common-args = {
    inherit
      pkgs
      overlays
      lib
      home-manager
      system
      username
      inputs
      self
      ;
  };
  overlays = import ../../overlays common-args;
in {
  nixpkgs = {
    config = {
      allowUnfree = true;
    };
    overlays = [
      overlays.hyproverlay
      overlays.flutteroverlay
      overlays.openvpn
    ];
  };

  nix = {
    settings = {
      trusted-users = ["jakob"];
      experimental-features = ["nix-command" "flakes" "pipe-operator"];
      auto-optimise-store = true;
      substituters = ["https://nix-community.cachix.org"];
      trusted-public-keys = ["nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="];
    };
    extraOptions = ''
      builders-use-substitutes = true
      secret-key-files = /etc/nix/cache-priv-key.pem
    '';
    gc = {
      automatic = true;
      dates = "monthly";
      options = "--delete-older-than 7d";
    };
  };
}

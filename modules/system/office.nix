{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.modules.office;
in {
  options.modules.office = {
    enable = lib.mkEnableOption "enable office";
    latex = lib.mkEnableOption "enable latex";
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages =
      [
        pkgs.musescore
        pkgs.gimp
        pkgs.libreoffice
        (pkgs.symlinkJoin {
          name = "vlc";
          paths = [pkgs.vlc];
          buildInputs = [pkgs.makeWrapper];
          postBuild =
            /*
            bash
            */
            ''
              wrapProgram $out/bin/vlc \
              --unset DISPLAY
            '';
        })
      ]
      ++ lib.optionals cfg.latex [pkgs.texlive.combined.scheme-full];
  };
}

{
  pkgs,
  username,
  ...
}: {
  # Set time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.utf8";
    LC_IDENTIFICATION = "de_DE.utf8";
    LC_MEASUREMENT = "de_DE.utf8";
    LC_MONETARY = "de_DE.utf8";
    LC_NAME = "de_DE.utf8";
    LC_NUMERIC = "de_DE.utf8";
    LC_PAPER = "de_DE.utf8";
    LC_TELEPHONE = "de_DE.utf8";
    LC_TIME = "de_DE.utf8";
  };

  # Enable the display Manager
  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
  };

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Define a user account.
  users.users.${username} = {
    isNormalUser = true;
    extraGroups = ["networkmanager" "wheel" "input" "video"];
    initialPassword = "1234";
  };

  users.defaultUserShell = pkgs.fish;
  security.sudo.extraConfig = "Defaults pwfeedback";
  programs.fish.enable = true;
  environment.shells = with pkgs; [fish];

  hardware = {
    opentabletdriver.enable = true;
    opentabletdriver.daemon.enable = true;
  };

  environment.pathsToLink = ["/share/xdg-desktop-portal" "/share/applications"];

  services = {
    tailscale = {
      enable = true;
      openFirewall = true;
    };
    # smartcard daemon for yubikey
    pcscd.enable = true;

    xserver = {
      xkb = {
        layout = "de";
      };
    };
  };

  # Configure console keymap
  console = {
    keyMap = "de";
  };

  fonts.packages = with pkgs; [
    nerd-fonts.fira-code
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}

{
  config,
  lib,
  fingerprint-sensor,
  ...
}: let
  cfg = config.modules.fingerprint;
in {
  # there are two modules which need to be imported in hosts.nix
  #
  imports = [
    fingerprint-sensor.nixosModules.open-fprintd
    fingerprint-sensor.nixosModules.python-validity
  ];

  options.modules.fingerprint = {
    enable = lib.mkEnableOption "enable fingerprint on X1 Carbon gen 6";
  };

  config = lib.mkIf cfg.enable {
    security.pam.services = {
      swaylock.fprintAuth = true;
      sudo.fprintAuth = true;
    };

    services = {
      open-fprintd.enable = true;
      python-validity.enable = true;
    };

    systemd.services = let
      targets = ["suspend.target" "hibernate.target" "hybrid-sleep.target" "suspend-then-hibernate.target"];
    in {
      python3-validity.enable = true;
      open-fprintd-resume.enable = true;
      open-fprintd-suspend.enable = true;
      open-fprintd-suspend.wantedBy = targets;
      open-fprintd-resume.wantedBy = targets;
      fprint-resume = {
        description = "restarts fingerprint services after suspend";
        after = targets;
        wantedBy = targets;
        unitConfig = {};
        serviceConfig = {
          Type = "oneshot";
          ExecStart = "systemctl restart open-fprintd python3-validity";
        };
      };
    };
  };
}

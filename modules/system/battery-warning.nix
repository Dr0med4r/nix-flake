{
  lib,
  config,
  pkgs,
  username,
  ...
}: let
  cfg = config.modules.battery-warning;
in {
  options.modules.battery-warning = {
    enable = lib.mkEnableOption "enable battery warning";
  };

  config = lib.mkIf cfg.enable {
    systemd.services = {
      battery-warning = {
        wantedBy = ["multi-user.target"];
        serviceConfig = {
          User = username;
          Type = "exec";
          Environment = ["DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"];
          ExecStart = pkgs.writeShellScript "battery-warning.sh" ''
            BATTERY=/sys/class/power_supply/BAT0
            while :; do
              if [ ! -f /tmp/last_percentage ];
              then cat $BATTERY/capacity > /tmp/last_percentage;
              fi
              PERCENTAGE=$(cat $BATTERY/capacity)
              LAST_PERCENTAGE=$(cat /tmp/last_percentage)
              CHARGING=$(cat $BATTERY/status)
              if [ $PERCENTAGE -lt 15 ] && [ $CHARGING != "Charging" ];
              then
                if [ ! $LAST_PERCENTAGE -eq $PERCENTAGE ];
                then
                ${pkgs.libnotify}/bin/notify-send "battery is low
              only $PERCENTAGE% remaining" --icon=nix-snowflake -u critical;
                echo $PERCENTAGE > /tmp/last_percentage;
                fi
              else echo $PERCENTAGE > /tmp/last_percentage;
              fi
              sleep 10s
            done
          '';
        };
      };
    };
  };
}

{
  pkgs,
  inputs,
  system,
  ...
}: {
  programs = {
    nix-ld.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  programs.kdeconnect.enable = true;

  environment.systemPackages = with pkgs; [
    # applications
    anki
    obs-studio
    openvpn
    inputs.zen-browser.packages."${system}".default
    thunderbird
    firefox
    obsidian
    kdePackages.gwenview
    kdePackages.okular
    nautilus
    opentabletdriver
    signal-desktop
    hicolor-icon-theme
    yubioath-flutter
  ];
}

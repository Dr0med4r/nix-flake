{
  config,
  pkgs,
  lib,
  username,
  ...
}: let
  cfg = config.modules.console-applications;
in {
  options.modules.console-applications = {
    enable = lib.mkEnableOption "enabble console-applications";
  };
  config = lib.mkIf cfg.enable {
    programs = {
      nh = {
        enable = true;
        flake = "/home/${username}/nix-flake";
      };
      direnv = {
        enable = true;
      };
      nix-index-database.comma.enable = true;
      git = {
        enable = true;
        config = {
          commit.verbose = true;
          alias = {
            diffs = "diff --staged";
            unstage = "restore --staged";
            ps = "push";
            pl = "pull";
            co = "checkout";
          };
          merge.tool = "vimdiff";
          pull.rebase = true;
          user.name = "dr0med4r";
          http.postbuffer = "1028m";
        };
      };
    };

    environment.systemPackages = with pkgs; [
      zellij
      pciutils
      usbutils
      xcowsay
      freshfetch
      img2pdf
      unzip
      wget
      git
      file
      unixtools.xxd
      zip
      zoxide
      ripgrep
      fd
      alejandra
      fishPlugins.hydro
      sl
      nnn
      btop
      kalker
    ];
  };
}

{
  username,
  lib,
  config,
  ...
}: let
  cfg = config.home-modules.copyq;
in {
  options = {
    home-modules.copyq.enable = lib.mkEnableOption "enable stylix copyq";
  };
  config = lib.mkIf cfg.enable {
    home-manager.users.${username}.
    xdg.configFile."copyq/themes/stylix.ini".text = lib.generators.toINI {} {
      general = with config.lib.stylix.colors.withHashtag; {
        fg = base05;
        bg = base00;
        find_fg = base04;
        find_bg = base01;
        sel_fg = base05;
        sel_bg = base02;
        edit_fg = base04;
        edit_bg = base01;
        alt_bg = base01;
        num_fg = base04;
        notes_bg = base01;
        notes_fg = base04;
      };
    };
  };
}

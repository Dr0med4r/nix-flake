{
  config,
  pkgs,
  lib,
  username,
  inputs,
  system,
  ...
}: let
  cfg = config.home-modules.hyprland;
  hyprlandpkgs = inputs.hyprland.packages.${system};
in {
  imports = [
    ./wlogout.nix
    ./waybar.nix
    ./fuzzel.nix
    ./dunst.nix
    ./swaylock.nix
    ./copyq.nix
    ./ags.nix
    ./hyprconf.nix
    ./keybinds.nix
    ./monitors.nix
  ];

  options.home-modules.hyprland = {
    enable = lib.mkEnableOption "enable hyprland";
  };

  config = lib.mkIf cfg.enable {
    home-modules.copyq.enable = true;
    home-manager.users.${username} = {
      services = {
        dunst.enable = true;
      };
      programs = {
        wlogout.enable = true;
        waybar.enable = true;
        fuzzel.enable = true;
        swaylock.enable = true;
        ags.enable = true;
      };
      home.packages = with pkgs; [
        copyq
        slurp
        grim
        hyprpicker
        hyprpaper
        libsForQt5.qt5.qtwayland
        brightnessctl
        libnotify
        pavucontrol
        hyprshade
      ];
      xdg.configFile = {
        "hypr/rotate.sh".source = ./rotate.sh;
        "hypr/show-audio.sh" = {
          executable = true;
          text =
            # bash
            ''
              notify-send "Volume:" -h int:value:$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | sed -E "s/.*([0-9])\.([0-9]{2,3})/\1\2/") -t 700
            '';
        };
        "hypr/show-light.sh" = {
          executable = true;
          text =
            # bash
            ''
              notify-send "Brightness: " -h int:value:"`brightnessctl -m | awk -F, '{print substr($4, 0, length($4)-1)}'`" -t 700
            '';
        };
        "hypr/hyprpaper.conf".text = import ./hyprpaper.nix {inherit lib;};
      };
      wayland.windowManager.hyprland = {
        enable = true;
        package = hyprlandpkgs.hyprland;
        systemd.variables = ["--all"];
        plugins = [
          inputs.hypr-darkwindow.packages.${system}.Hypr-DarkWindow
          # inputs.hyprspace.packages.${system}.Hyprspace
        ];
      };
    };
  };
}

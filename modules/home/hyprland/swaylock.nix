{
  pkgs,
  username,
  ...
}: {
  home-manager.users.${username} = {
    programs.swaylock = {
      package = pkgs.swaylock-effects;
      settings = {
        indicator = true;
        clock = true;
        effect-blur = "7x2";
        screenshots = true;
        effect-scale = 0.3;
        time-effects = true;
      };
    };
  };
}

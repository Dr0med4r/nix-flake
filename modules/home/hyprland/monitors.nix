{
  pkgs,
  inputs,
  system,
  username,
  ...
}: let
  hyprpkgs = inputs.hyprland.packages.${system};
in {
  home-manager.users.${username} = {
    xdg.configFile = {
      "fish/completions/monitors.fish".text =
        # fish
        ''
          function monitor_names
              set -l names (${hyprpkgs.hyprland}/bin/hyprctl monitors -j| ${pkgs.jq}/bin/jq -r .[].name)
              for name in $names
                  echo $name
              end
          end

          function resolutions
              set -l name (commandline -opc)
              set -e name[1]
              set -l resolutions ( ${hyprpkgs.hyprland}/bin/hyprctl monitors -j | ${pkgs.jq}/bin/jq -r "map({key: .name, value: .availableModes })| from_entries | .[\"$name[1]\"] | .[]" )
              for resolution in $resolutions
                  echo $resolution
              end
          end


          set positions auto

          function scale
              echo 1
              set -l name (commandline -opc)
              set -e name[1]
              echo ( ${hyprpkgs.hyprland}/bin/hyprctl monitors -j | ${pkgs.jq}/bin/jq -r "map({key: .name, value: .scale })| from_entries | .[\"$name[1]\"]" )
          end


          set extras mirror (monitor_names)



          complete -c monitors -f -n "test (count (commandline -opc)) -eq 1" -a "(monitor_names)" -d "monitor names"

          complete -c monitors -f -n "test (count (commandline -opc)) -eq 2" -a "(resolutions)" -d "resolutions"

          complete -c monitors -f -n "test (count (commandline -opc)) -eq 3" -a "$positions" -d "positions"

          complete -c monitors -f -n "test (count (commandline -opc)) -eq 4" -a "(scale)" -d "scale"

          complete -c monitors -f -n "test (count (commandline -opc)) -ge 5" -a "$extras" -d "extras"
        '';
      "fish/functions/monitors.fish".text =
        #fish
        ''
          function monitors
              if test (count $argv) -lt 4
                  echo "not enough arguments"
                  return
              end
              set name $argv[1]
              set resolution $argv[2]
              set pos $argv[3]
              set scale $argv[4]
              set extra (string join ", " -- $argv[5..])
              ${hyprpkgs.hyprland}/bin/hyprctl keyword monitor $name, $resolution, $pos, $scale, $extra
          end
        '';
    };
  };
}

{
  config,
  lib,
  username,
  ...
}: let
  cfg = config.home-modules.hyprland;
in {
  config = lib.mkIf cfg.enable {
    home-manager.users.${username} = {
      wayland.windowManager.hyprland = {
        extraConfig = ''
          submap = resize

          # sets repeatable binds for resizing the active window
          bindel = , right , resizeactive , 10  0
          bindel = , left  , resizeactive , -10 0
          bindel = , up    , resizeactive , 0   -10
          bindel = , down  , resizeactive , 0   10
          bindel = , l     , resizeactive , 10  0
          bindel = , h     , resizeactive , -10 0
          bindel = , k     , resizeactive , 0   -10
          bindel = , j     , resizeactive , 0   10

          # use reset to go back to the global submap
          bind = , escape , submap , reset
          bind = SUPER, R, submap, reset

           submap = reset
        '';
        settings = {
          bindel = [
            ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+ && ~/.config/hypr/show-audio.sh"
            ", XF86AudioLowerVolume, exec, wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%- && ~/.config/hypr/show-audio.sh"
            ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
            ", XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"
            ", XF86MonBrightnessUp   , exec , brightnessctl set +5% && ~/.config/hypr/show-light.sh"
            ", XF86MonBrightnessDown , exec , brightnessctl set 5%- && ~/.config/hypr/show-light.sh"
          ];

          bindl = [", switch:Lid Switch, exec, swaylock"];

          bindm = [
            "${cfg.mainMod}, mouse:272, movewindow"
            "${cfg.mainMod}, mouse:273, resizewindow"
          ];

          bind =
            [
              # plugins
              # "${cfg.mainMod}, Tab, overview:toggle"
              "${cfg.mainMod} SHIFT, O, invertactivewindow "

              # logout
              ", XF86PowerOff, exec, wlogout"

              # clipboard
              ''${cfg.mainMod}, Print, execr, grim -g "$(slurp)" - | wl-copy''
              "${cfg.mainMod}, V, exec, copyq toggle"

              "${cfg.mainMod}, W, exec, pkill -SIGUSR1 waybar"

              # programs
              "${cfg.mainMod}, T, exec, alacritty"
              "${cfg.mainMod}, Q, killactive, "
              "${cfg.mainMod} SHIFT, M, exit, "
              "${cfg.mainMod}, E, exec, nautilus"
              "${cfg.mainMod}, B, exec, zen"
              "ALT, SPACE, exec, fuzzel"

              # layout
              "${cfg.mainMod}, F, togglefloating, "
              #"${cfg.mainMod}, P, pseudo, # dwindle"
              "${cfg.mainMod}, S, togglesplit, # dwindle"
              "${cfg.mainMod}, F11, fullscreen"
              "${cfg.mainMod}, N, swapnext"
              "${cfg.mainMod}, P, swapnext, prev"

              # Move focus with {cfg.mainMod} + arrow keys
              "${cfg.mainMod}, h, movefocus, l"
              "${cfg.mainMod}, l, movefocus, r"
              "${cfg.mainMod}, k, movefocus, u"
              "${cfg.mainMod}, j, movefocus, d"

              # change rotation
              "${cfg.mainMod}, up, exec, ~/.config/hypr/rotate.sh 0 ${cfg.scale}"
              "${cfg.mainMod}, left, exec, ~/.config/hypr/rotate.sh 1 ${cfg.scale}"
              "${cfg.mainMod}, right, exec, ~/.config/hypr/rotate.sh 3 ${cfg.scale}"
              "${cfg.mainMod}, down, exec, ~/.config/hypr/rotate.sh 2 ${cfg.scale}"

              # move windows between workspaces
              "${cfg.mainMod} CTRL, left, workspace, e-1"
              "${cfg.mainMod} CTRL, right, workspace, e+1"
              "${cfg.mainMod} CTRL SHIFT, left, movetoworkspace, -1"
              "${cfg.mainMod} CTRL SHIFT, right, movetoworkspace, +1"

              # move windows between monitors
              "${cfg.mainMod} SHIFT, n, movecurrentworkspacetomonitor, +1"

              "${cfg.mainMod}, I, togglespecialworkspace, scratch"
              "${cfg.mainMod} SHIFT, I, movetoworkspace, special:scratch"

              # Scroll through existing workspaces with {cfg.mainMod} + scroll
              "${cfg.mainMod}, mouse_down, workspace, e+1"
              "${cfg.mainMod}, mouse_up, workspace, e-1"

              # submap
              "${cfg.mainMod}, R, submap, resize"

              # shaders
              "${cfg.mainMod}, O, exec, hyprshade toggle ${./shaders/invert-colors.glsl}"
            ]
            ++
            # Switch workspaces with {cfg.mainMod} + [0-9]
            # Move active window to a workspace with {cfg.mainMod} + SHIFT + [0-9]
            builtins.concatLists (builtins.genList (x: let
                ws = toString (
                  if x == 9
                  then 0
                  else x + 1
                );
              in [
                "${cfg.mainMod}, ${ws}, workspace, ${toString (x + 1)}"
                "${cfg.mainMod} SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
              ])
              10);
        };
      };
    };
  };
}

{username, ...}: {
  home-manager.users.${username}.programs.fuzzel.settings = {
    main = {
      terminal = "$TERM -e";
      show-actions = true;
      list-executables-in-path = true;
      filter-desktop = false;
    };
  };
}

{
  config,
  lib,
  username,
  pkgs,
  ...
}: let
  cfg = config.home-modules.hyprland;
in {
  options.home-modules.hyprland = {
    nvidia = lib.mkEnableOption "enable nvidia";
    scale = lib.mkOption {
      type = lib.types.str;
      default = "1.2";
    };
    mainMod = lib.mkOption {
      type = lib.types.str;
      default = "SUPER";
    };
    monitors = lib.mkOption {
      type = lib.types.listOf lib.types.str;
      default = [];
    };
    extraDevices = lib.mkOption {
      type = lib.types.listOf (lib.types.attrsOf lib.types.str);
      default = [];
    };
  };

  config = lib.mkIf cfg.enable {
    home-manager.users.${username} = {
      wayland.windowManager.hyprland = {
        settings = lib.mkMerge [
          {
            source = "./test.conf";
            exec-once = [
              "waybar"
              "dunst"
              "${pkgs.kdePackages.polkit-kde-agent-1}/libexec/polkit-kde-authentication-agent-1"
              "otd-daemon"
              "nm-applet"
              "copyq"
              "hyprctl setcursor breeze_cursors 24"
              "hyprpaper"
            ];

            workspace = [
              "special:scratch, gapsout:60, gapsin:0, on-created-empty:exec alacritty, rounding:0"
            ];

            monitor = [",preferred,auto,1"] ++ cfg.monitors;

            # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
            general = {
              gaps_in = 5;
              gaps_out = 10;
              border_size = 2;
              layout = "dwindle";
              resize_on_border = true;
              snap = {
                enabled = true;
                window_gap = 5;
                monitor_gap = 10;
              };
            };

            decoration = {
              blur = {
                enabled = true;
                size = 3;
                passes = 1;
                new_optimizations = true;
              };
              rounding = 5;
            };

            animations = {
              enabled = true;
              bezier = [
                "wind, 0.05, 0.9, 0.1, 1.05"
                "winIn, 0.4, 1.1, 0.6, 1.15"
                "winOut, 0.3, -0.3, 0, 1"
                "liner, 1, 1, 1, 1"
              ];
              animation = [
                "windows, 1, 6, wind, slide"
                "windowsIn, 1, 6, winIn, slide"
                "windowsOut, 1, 5, winOut, slide"
                "windowsMove, 1, 5, wind, slide"
                "border, 1, 1, liner"
                "borderangle, 1, 30, liner, loop"
                "fade, 1, 10, default"
                "workspaces, 1, 5, wind, slide"
                "specialWorkspace, 1, 5, wind, slidevert"
              ];
            };

            device =
              [
                {
                  name = "ydotoold-virtual-device";
                  kb_layout = "us";
                  kb_options = "";
                }
                {
                  name = "sonix-usb-device";
                  kb_layout = "de";
                }
                {
                  name = "at-translated-set-2-keyboard";
                  kb_variant = "e2";
                }
              ]
              ++ cfg.extraDevices;
            input = {
              kb_layout = "de";
              follow_mouse = 1;
              numlock_by_default = true;

              touchpad = {
                natural_scroll = true;
                disable_while_typing = false;
              };
              sensitivity = 0; # -1.0 - 1.0, 0 means no modification.
            };

            gestures = {
              workspace_swipe = true;
              workspace_swipe_fingers = 3;
              workspace_swipe_distance = 100;
              workspace_swipe_min_speed_to_force = 15;
            };

            misc = {
              force_default_wallpaper = 1;
              disable_hyprland_logo = true;
              disable_autoreload = true;
              initial_workspace_tracking = false;
              enable_swallow = true;
              swallow_regex = "(Alacritty)";
            };

            xwayland.force_zero_scaling = true;

            dwindle = {
              pseudotile = true;
              preserve_split = true;
            };

            windowrule = let
              pavucontrolRegex = "^(org.pulseaudio.pavucontrol)$";
              copyqRegex = "^(com.github.hluk.copyq)$";
            in [
              "float, ${pavucontrolRegex}"
              "size 40% 30%, ${pavucontrolRegex}"
              "move 30% 7%, ${pavucontrolRegex}"
              "float, ${copyqRegex}"
              "size 30% 40%, ${copyqRegex}"
              "move onscreen cursor -50% -70%, ${copyqRegex}"
            ];
          }
          (lib.mkIf cfg.nvidia {
            render.explicit_sync = 1;
            cursor.no_hardware_cursors = true;
          })
        ];
      };
    };
  };
}

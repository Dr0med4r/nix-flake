{username, ...}: {
  home-manager.users.${username}.services.dunst.settings = {
    global = {
      origin = "top-right";
      monitor = 0;

      follow = "mouse";

      width = 300;
      height = 300;

      offset = "20x40";

      scale = 0;
      notification_limit = 8;

      progress_bar = true;
      progress_bar_height = 10;
      progress_bar_frame_width = 0;
      progress_bar_min_width = 150;
      progress_bar_max_width = 300;
      #-----------------------highlight = "#0AF5AD";
      progress_bar_corner_radius = 2;

      gap_size = 2;
      padding = 9;
      horizontal_padding = 13;
      frame_width = 2;
      sort = "yes";
      idle_threshold = 0;
      line_height = 0;
      markup = "full";
      format = "<b>%s</b>\\n%b";
      alignment = "left";
      vertical_alignment = "center";
      show_age_threshold = -1;
      word_wrap = "yes";
      stack_duplicates = "true";
      hide_duplicate_count = "false";
      show_indicators = "yes";
      icon_position = "left";
      min_icon_size = 0;
      max_icon_size = 32;
      sticky_history = "false";
      history_length = 0;
      corner_radius = 10;
      indicate_hidden = "yes";
      title = "Dunst";
      class = "dunst";
      mouse_left_click = "close_current";
      mouse_middle_click = "do_action, close_current";
      mouse_right_click = "close_all";
    };
    urgency_critical = {
      timeout = 0;
    };
  };
}

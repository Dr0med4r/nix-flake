{
  username,
  inputs,
  ...
}: {
  imports = [
    ./stylix.nix
    ./common-home.nix
    ./alacritty.nix
    ./hyprland
    ./zellij.nix
  ];
  home-manager.users.${username} = {
    imports = [
      inputs.ags.homeManagerModules.default
    ];
  };
}

{
  pkgs,
  username,
  ...
}: {
  homeManagerModules.nvim = {
    enable = true;
    neovide = true;
  };
  home-manager.backupFileExtension = "bak";
  home-manager.users.${username} = {
    programs.home-manager.enable = true;

    xdg = {
      mime.enable = true;
    };
    programs = {
      fish = {
        enable = true;
        shellInit = import ../../config/fishconf.nix {inherit pkgs;};
      };
      zoxide = {
        enable = true;
        enableFishIntegration = true;
      };
    };
    home = {
      stateVersion = "23.05";
    };
  };
}

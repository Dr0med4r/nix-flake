{
  lib,
  config,
  pkgs,
  username,
  ...
}: let
  cfg = config.home-modules.stylix;
in {
  options.home-modules.stylix = {
    enable = lib.mkEnableOption "enable stylix";
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = [pkgs.adw-gtk3 pkgs.kdePackages.breeze-icons];
    stylix = {
      enable = true;
      image = builtins.fetchurl {
        url = "https://w.wallhaven.cc/full/ex/wallhaven-ex9gwo.png";
        sha256 = "1xaqgn8n56q77ji22sf5134vqxbhgb8g6bw7rhr55bclnmzjdgph";
      };
      polarity = "dark";
      base16Scheme = "${pkgs.base16-schemes}/share/themes/tomorrow-night.yaml";
      cursor = {
        package = pkgs.kdePackages.breeze;
        name = "breeze_cursors";
        size = 24;
      };
      opacity = {
        terminal = 0.8;
        desktop = 0.2;
        popups = 0.9;
      };
    };

    home-manager.users.${username} = {
      stylix = {
        targets = {
          kde.enable = true;
        };
      };
      gtk = {
        iconTheme = {
          name = "breeze-dark";
          package = pkgs.kdePackages.breeze-icons;
        };
      };
    };
  };
}

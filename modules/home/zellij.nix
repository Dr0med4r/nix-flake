{
  config,
  lib,
  username,
  ...
}: let
  cfg = config.home-modules.zellij;
in {
  options.home-modules.zellij = {
    enable = lib.mkEnableOption "enable zellij";
  };
  config = lib.mkIf cfg.enable {
    home-manager.users.${username} = {
      xdg.configFile."zellij/config.kdl".source = ../../config/zellij.kdl;
      programs.zellij = {
        enable = true;
      };
    };
  };
}

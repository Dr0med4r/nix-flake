{
  config,
  lib,
  username,
  ...
}: let
  cfg = config.home-modules.alacritty;
in {
  options.home-modules.alacritty = {
    enable = lib.mkEnableOption "enable alacritty";
  };
  config = lib.mkIf cfg.enable {
    home-manager.users.${username} = {
      programs.alacritty = {
        enable = true;
        settings = import ../../config/alacrittysettings.nix;
      };
    };
  };
}

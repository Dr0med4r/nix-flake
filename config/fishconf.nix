{...}:
/*
fish
*/
''
  set --global hydro_color_git green
  set --global hydro_color_pwd blue
  set --global hydro_symbol_git_dirty (set_color yellow)•(set_color $hydro_color_git)

  export XDG_DATA_DIRS=$XDG_DATA_DIRS:/usr/share:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share

  function rebuild-switch -a host;
    sudo nixos-rebuild --flake ~/nix-flake#$host switch --fallback --show-trace $argv[2..-1]
  end;



  function n --wraps nnn --description 'support nnn quit and change directory'
      # Block nesting of nnn in subshells
      if test -n "$NNNLVL" -a "$NNNLVL" -ge 1
          echo "nnn is already running"
          return
      end

      # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
      # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
      # see. To cd on quit only on ^G, remove the "-x" from both lines below,
      # without changing the paths.
      if test -n "$XDG_CONFIG_HOME"
          set -x NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
      else
          set -x NNN_TMPFILE "$HOME/.config/nnn/.lastd"
      end

      # The command function allows one to alias this function to `nnn` without
      # making an infinitely recursive alias
      command nnn -e $argv

      if test -e $NNN_TMPFILE
          source $NNN_TMPFILE
          rm $NNN_TMPFILE
      end
  end
''
